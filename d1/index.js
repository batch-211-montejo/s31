// NODE JS INTRODUCTION

// Use the "require" directive To load the Noede.js modules
// A "module" is a software component or part of a program that contains one or more routines
// the "http module" lets Node.js transfer data using the Hypertext Transfer Protocol
// Tha "http module" is a set of individual files that contain code to create "component" that helps establibh data transfer between applications
// HTTPS is a protocol that allow the fetching of resources such as HTML docs
// HTTP is the one allowing HTML to be seen
//clients (browsers) and servers(Node.js/Express.js application) communicate by exhanging individual messages
// the message sent by the client, usually, a web browser, are called requests
// the messages sent by the server as an answer are called responses 

let http = require("http")

// the http module has a create Server method that accepts a function as an argument and allows for a creation of a server
// The arguments passed in the function are request and response objects that contains methods that allow us to receive requests from the client and send responses back to it

http.createServer (function(request, response) {

	/*
		Use the writeHead() method to:
		-set a status code for the response - a 200 means OK
		-set the content-type of the response as a plain text message
	*/
	response.writeHead(200, {'Content-Type': 'text/plain'})

	// send the response with a text content "Hello World"
	response.end('Hello World')
}).listen(4000) //port

// a port is a virtual point where network connectons start and end
// each port is associated with a specific process or service
// the server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it, eventually communicatiang with our server

console.log('Server running at localhost:4000');



