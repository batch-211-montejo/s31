const http = require('http');

// create a variable "port" to store the port number 
const port = 4000;

// creates a variable "server" that sotres the output of the "createServer" method
const server = http.createServer((request, response)=> {
		if (request.url == '/greeting'){
			response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end('Hello again')
		} else if (request.url == '/homepage') {
			response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end('This is the Home Page')
		} else {
			response.writeHead(404, {'Content-Type': 'text/plain'})
			response.end('Page not available')
		}
	})

// uses the "server" and "port" variables created above
server.listen(port)

// when server is running, console will print the message
console.log(`Server now accessible at localhost:${port}.`)